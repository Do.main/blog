<center><h1> 从零开始搭建自动构建、测试、发布、部署系统</h1></center>

在游戏服务器的工作中，每日版本的构建、测试、发布和部署是_必不可少_ 的日常任务，却又是十分**枯燥繁琐**的一项工作。这项工作一般称之为[CI/CD系统](https://www.infoworld.com/article/3271126/what-is-cicd-continuous-integration-and-continuous-delivery-explained.html)。为了解脱这种痛苦，现在有很多现成的系统可用。常见的托管网站 [github](https://docs.github.com/en/actions/guides/about-continuous-integration)、[gitlab](https://docs.gitlab.com/ee/ci/)、[coding](https://coding.net/products/ci) 等，都提供CI/CD服务。独立的CI/CD系统又有 [Jenkins](https://www.jenkins.io/)、[CircleCI](https://circleci.com/)、[Travis CI](https://www.travis-ci.com/) 等等，可谓相当丰富。然而这篇文章和这些统统**_没有关系_** 。为什么？因为我都没用过啊！更重要的是我目前的开发环境属于研发网，是没有Internet访问权限的，这些东西大多高度依赖网络，就算可以离线安装，安装依赖关系也要折腾半天，而且很多功能对我来说根本就用不上。因此就有了这篇文章：如何从零开始搭建一个简易CI/CD系统。

在介绍之前，我先说一下我们的开发流程。我们开发依然使用[SVN]([Apache Subversion](https://subversion.apache.org/))作为仓库，因为大家更加熟悉。主要的SVN有四个：服务器代码（Server），客户端资源（Client），客户端逻辑代码（Core），服务器可执行程序（ServerBin）。最有一个SVN其实是为了使用方便，可以快速回滚到指定的版本，而不需要重新编译。当前三个仓库发生改变时，需要自动触发我们的CI/CD系统，最终大家就可以获取到最新的版本进行测试。简易流程图如下：

```mermaid
graph LR
commit([仓库有改变])
build[自动构建]
compile{编译通过}
test{测试通过}
error([通知错误])
publish[发布到同步服]
deploy([部署到测试服])

commit --> build --> compile
compile -- 是 --> test 
compile -- 否 --> error
test -- 是 --> publish --> deploy
test -- 否 --> error
```

## 构建

### 检查提交

自动构建是自动化CI/CD系统的第一步，当源代码有改变时，就需要通知构建系统自动拉取代码进行编译。由于我们使用的是SVN，这一步就可以通过Hook来实现。这里主要用到的是 pre-commit 和 post-commit 两个钩子，其中前者用来检查提交的日志是否符合要求，是否关联了具体的 issue，assets 和 meta 文件是否同时提交或删除，文件属性是否有错等等，可以过滤掉大部分不应该的错误。后者就是用来通知构建系统开始构建了。流程如下：

```mermaid
graph LR
commit([提交SVN])
pre-commit[检查规范]
check{检查通过}
reject([拒绝提交])
notify([通知构建系统])

commit --> pre-commit --> check
check -- 是 --> notify
check -- 否 --> reject
```

资源的提交虽然不需要编译，但是也会出发构建系统去测试。因为测试需要的时间较长，所以这一步没有通过 pre-commit 钩子来拒绝错误的提交，错误的提交会在后面测试失败时显示出来。

```bash
#!/bin/bash
REPOS="$1"
TXN="$2"
SVNLOOK=/usr/bin/svnlook

function check_issue {
	# 检查提交的日志里面是否包含了具体的issue网址，这里使用teambition
	local msg=`$SVNLOOK log -t "$TXN" "$REPOS"`
	if [[ ! "$msg" =~ https://www\.teambition\.com/task/[0-9a-fA-F]+ ]]; then
		echo "请关联teambition任务" 1>&2
		exit 1
	fi
}

function check_meta {
	# 如果提交的是unity的assets文件，确保相关的meta文件一起提交了
	local files=`$SVNLOOK changed -t "$TXN" "$REPOS" | grep -E "^[AD]\s+.*/Assets/.*" | grep -v "\.meta$"`
	local metas=`$SVNLOOK changed -t "$TXN" "$REPOS" | grep -E "^[AD]\s+.*/Assets/.*" | grep "\.meta$ | sed 's/\.meta$//"`
	local err=`sort <(echo "$files") <(echo "$metas") | uniq -u`
	
	if [ -n "$err" ]; then
		echo "以下文件必须和meta文件一起提交：" 1>&2
		echo "$err" 1>&2
		exit 2
	fi
}

function check_conflict {
	# 不要提交冲突文件
	local conflict=`$SVNLOOK changed -t "$TXN" "$REPOS" | grep -E "^A\s+.+\.(mine|r[0-9]+)(\.meta)?$"`
	if [ -n "$conflict" ]; then
		echo "请不要提交冲突文件：" 1>&2
		echo "$conflict" 1>&2
		exit 3
	fi
}

function check_size {
	local max_mb=$((50))
	local max_size=$(($max_mb * 1024 * 1024)) # 最大允许上传50MB文件
	
	$SVNLOOK changed -t "$TXN" "$REPOS" | while read status file
	do
		[[ $status != "A" ]] && continue  # 只检查添加文件
		[[ $file == */ ]] && continue     # 只检查文件
		local size=`$SVNLOOK filesize -t "$TXN" "$REPOS" "$file"`
		if [[ $size -gt $max_size ]]; then
			echo "文件 '$file' 太大了，最大限制 $max_mb MB" 1>&2
			exit 4
		fi
	done
	
	[ ${PIPESTATUS[1]} == 0 ] || exit 2 # 注意上面用了管道来处理，需要检查 PIPESTATUS 的值
}

check_issue
check_meta
check_conflict
check_size
```
post-commit 钩子用来通知自动构建系统有改变，很简单，就是把当前版本号写入对应的文件就行了，当然了，事先需要配置好无密码登录ssh，这里就不多说了。

```bash
#!/bin/bash
REPOS="$1"
REV="$2"
TXN_NAME="$3"

project=`basename "$REPOS"`
IFS='/' read -r branch root path <<< "$(svnlook changed -r $REV $REPOS | head -n 1 | awk '{print $2}')"
[ "$branch" = "branches" ] && branch=$root
ssh builder@192.168.3.185 "echo $REV > /home/builder/svn/$project/$branch"
```
### 自动构建

通过检查的提交通知到构建系统之后，系统需要进行自动构建的调度。前面的 post-commit 脚本会在构建的机器上生成一个文件，里面是这次提交的版本号，而构建系统会记录上一次成功编译的版本号，如果两者不一致，说明需要重新编译。因为我们自动构建的机器只有一台，需要处理各种不同项目、不同分支的代码，因此不可能收到通知就立即进行构建，因为很可能之前的构建还没完成，需要放到队列里面等待。我使用 cron 的定时功能，每分钟检查一次，如果当前不在编译，而且代码有改变，就开始编译。为了方便查看编译的进度，我把编译放进 screen 中执行，可以随时查看。机器的简易状态如下：
``` mermaid
stateDiagram
[*] --> 检查
检查 --> 休眠
检查 --> 编译
检查 --> 退出
退出 --> 休眠
编译 --> 休眠
休眠 --> 检查
```
cron 的配置很简单，就是1分钟执行一次检查脚本：

```bash
*/1	*	*	*	*	source /home/builder/.bashrc; /home/builder/bin/build.sh
*/1	*	*	*	*	source /home/builder/.bashrc; /home/builder/bin/publish.sh
```

第一条就是检查是否需要构建，第二条就是检查是否需要发布到测试机器上。两者都是需要通过队列来处理，这里使用检查 screen 进程是否存在来实现：

```bash
#!/bin/bash

if ! screen -list | grep -q "build"; then
	cd /home/builder/svn
	screen -dmS build ./auto_build.sh
else
	screen -S build -p 0 -X stuff `printf "exit\r"`
fi
```

至于为什么会有 else 那一行，是因为我发现经常会出现编译脚本已经运行完了，但是 screen 的 session 不退出的问题，所以需要发送指令强制退出，这里用 [stuff]([Screen User's Manual (gnu.org)](https://www.gnu.org/software/screen/manual/screen.html#Paste)) 命令来实现退出，前面的 -p 0 也不可以少，否则对于 detached 的 session 是不起作用的，必须 attach 过至少一次才行。

```flow
start=>start: 开始
wait=>operation: 等待1分钟
check=>condition: 检查是否有改变
update=>operation: 更新
build=>operation: 编译
sucess=>condition: 编译成功
error=>operation: 通知错误
test=>operation: 通知测试

start->wait->check
check(yes)->update->build->sucess
check(no)->wait
sucess(yes)->test->wait
sucess(no)->error->wait
```
1. 第一步
2. 第二步

## 测试

可以首行缩进？

## 发布

## 部署
